% Checking functions for 9 x 9 Sudoku puzzles
function subfa
% function [solution] = subfa
% Main program. Only real input is the initial problem.

% Sets an example sudoku problem
% Problem (and solution available) at http://dingo.sbs.arizona.edu/~sandiway/sudoku/examples.html
% problem = [0,0,0,2,6,0,7,0,1;6,8,0,0,7,0,0,9,0;1,9,0,0,0,4,5,0,0;8,2,0,1,0,0,0,4,0;0,0,4,6,0,2,9,0,0;0,5,0,0,0,3,0,2,8;0,0,9,3,0,0,0,7,4;0,4,0,0,5,0,0,3,6;7,0,3,0,1,8,0,0,0];

% Another single-solution problem
% problem = [0,7,0,0,0,5,2,0,8;1,0,0,0,0,0,0,0,4;8,0,0,9,2,0,7,0,0;0,0,7,0,8,2,9,0,3;0,6,0,3,0,4,0,8,0;5,0,8,7,9,0,1,0,0;0,0,1,0,7,8,0,0,5;7,0,0,0,0,0,0,0,1;2,0,3,6,0,0,0,7,0];

% A problem with multiple solutions, found at http://www.sudokudragon.com/unsolvable.htm
problem = [0,8,0,0,0,9,7,4,3;0,5,0,0,0,8,0,1,0;0,1,0,0,0,0,0,0,0;8,0,0,0,0,5,0,0,0;0,0,0,8,0,4,0,0,0;0,0,0,3,0,0,0,0,6;0,0,0,0,0,0,0,7,0;0,3,0,5,0,0,0,8,0;9,7,2,4,0,0,0,5,0];

% Alternate test: using a zeroed problem
% problem = zeros(9);

%Debugging text
disp('The original problem:')
disp(problem)

% Actual program
disp('Permanent value matrix:')
[solidmatrix] = immutablesudoku(problem); %Set the immutable values matrix
disp(solidmatrix);
initrow = 1; % Initial input row is 1
initcol = 0; % Initial input column is 0, to account for the initial advancement of the advancesudoku function
[row,col] = advancesudoku(initrow,initcol,solidmatrix);
[solution,finalcorrect] = validcheck(row,col,problem,solidmatrix);
%disp(solution);
end


%% These functions are used to check the current problem for duplicates in rows.
% Inputs: Current position to check for adherence (row, col) and the problem matrix
% Outputs: Number of instances of the current position's number
% If the number is a valid value for that position, each counter value should be 1 at the end of the process.


%% Row Checker Function
function [counter] = rowchecker(row,col,problem)
counter = 0; %Set counter to zero
for x=1:1:9 % For all nine columns in the row
    if problem(row,x) == problem(row,col) % If the currently checked position equals the checked value
        counter = counter + 1; % Increase the counter by one
    end
end
end


%% Column Checker Function
function [counter] = colchecker(row,col,problem)
counter = 0; %Set counter to zero
for x=1:1:9 % For all nine rows in the column
    if problem(x,col) == problem(row,col) % If the currently checked position equals the checked value
        counter = counter + 1; % Increase the counter by one
    end
end
end


%% Box Checker Function
function [counter] = boxchecker(row,col,problem)
rowoffset = floor(row/3-.1)*3; %Set offset values for row
coloffset = floor(col/3-.1)*3; %Set offset values for column
counter = 0; %Set counter to zero
for x=1:1:3 % For the three rows in the box
    for y=1:1:3 % For the three columns in the box
        if problem(x+rowoffset,y+coloffset)==problem(row,col) % If the currently checked position equals the checked value
            counter = 1 + counter; % Increase the counter by one
        end
    end
end
end

%% This function advances the position of the program to the next changable index
function [nextrow,nextcol] = advancesudoku(row,col,validmatrix)
%Advances the sudoku solver to the next possible editable solution
nextcol = col + 1; %advance column
nextrow = row; %row has a possibility of staying the same
readyflag = 0; %set the ready flag to 0
while readyflag == 0;
    if nextcol == 10 %If column exceeds the bounds of the matrix
        nextcol = 1; % reset the column
        nextrow = row+1; %advance the column
    end
    if nextrow == 10 %If the row has a value of 10, the evaluation is over
        readyflag = 1; %The script is ready to proceed
    else %otherwise,
        if validmatrix(nextrow,nextcol) == 1 %If the validmatrix says that this position is unmutable
            % disp(['Row ',num2str(nextrow),' and column ',num2str(nextcol),' is unmutable. Moving on. '])
            readyflag = 0; %The script is not yet ready to proceed
            nextcol = nextcol + 1; %Advance the column again
        else %otherwise
            readyflag = 1; %We're ready to move on
        end
    end
end


end


%% This is the recursive function that actually solves the puzzle
function [problem,digitcorrect] = validcheck(row,col,problem,validmatrix)
digitcorrect = 0; %Sets digit flag as incorrect initially. Initial value of 0 is already incorrect.
%disp(['Current row: ',num2str(row)]) %debugging text
%disp(['Current column: ',num2str(col)]) %debugging text
nextdigitcorrect = 0; %initialize the solution-found check variable
while digitcorrect == 0 %While THIS digit is in question
    problem(row,col) = problem(row,col)+1; %Increment the digit value
    %disp(['Incrementing (',num2str(row),',',num2str(col),') to ',num2str(problem(row,col)),'.'])
    if problem(row,col) > 9 %If the digit is higher than 10, all possiblities were explored down this tree, no solution
        digitcorrect=2; %Marks a set of regression, some previous number was wrong
        problem(row,col)=0;
    else %If the number is a valid number (between 1 and 9)
        [rowvalid] = rowchecker(row,col,problem); %Check row for duplicates
        [colvalid] = colchecker(row,col,problem); %Check column for duplicates
        [boxvalid] = boxchecker(row,col,problem); %Check box for duplicates
        isvalid = rowvalid+colvalid+boxvalid;
        %disp([rowvalid,colvalid,boxvalid]);
        if rowvalid+colvalid+boxvalid == 3 %If the above is equal to three, the digit fits (for now)
            % disp('Requirements for digit have been met.')
            [nextrow,nextcol] = advancesudoku(row,col,validmatrix); %Calculate the next variable to iterate
            if nextrow == 10 %If "row 10" has been reached, we have reached a solution (?)
                digitcorrect=1; %Set the digit to reflect absolute correctness
                disp(['Row 10 reached. Possible Answer:'])
                disp(problem)
		    else %Otherwise, we need to advance the program to the next cell
                [problem,nextdigitcorrect] = validcheck(nextrow,nextcol,problem,validmatrix); %Run the function for the next row and column
            end
        end        
    end
%    if nextdigitcorrect == 1 %If the "Solution Found" Flag is passed down
%        digitcorrect = 1; %Pass down the "Solution Found" Flag
%    end
end
end

%% This takes a sudoku matrix and outputs a matrix of permanent locations
function [solidmatrix] = immutablesudoku(problem)
solidmatrix = zeros(9); %Initial zero matrix the size of the sudoku
for x=1:1:9 %For every row
    for y=1:1:9 %For every column 
        if problem(x,y) ~= 0 %If x,y in the problem is already non-zero
            solidmatrix(x,y) = 1; %Set the corresponding value in solidmatrix to 1
        end
    end
end
end

